<?
$config=array(
	"name" => "Размеры к товарам",
	"status" => "system",
	"windows" => array(
		"create" => array("width" => 400,"height" => 300),
		"edit" => array("width" => 400,"height" => 400),
		"list" => array("width" => 400,"height" => 500),
	),
	"right" => array("admin","#GRANTED"),
	"main_table" => "lt_good_sizes",
	"list" => array(
		"size" => array("isLink" => true),
	),
	"select" => array(
		"max_perpage" => 100,
		"default_orders" => array(
			array("size" => "ASC"),
		),
		"default" => array(
        ),
	),
);

?>